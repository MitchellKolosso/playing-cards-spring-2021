
// Ryan Appel
// Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

// enums for rank & suit
enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven,
	Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Spades, Hearts, Clubs, Diamonds
};


// struct for card
struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) {
    switch (card.rank) {
    case Ace: {
        cout << "The Ace";
        break;
    }
    case King:
        cout << "The King";
        break;
    case Queen:
        cout << "The Queen";
        break;
    case Jack:
        cout << "The Jack";
        break;
    case Ten:
        cout << "The Ten";
        break;
    case Nine:
        cout << "The Nine";
        break;
    case Eight:
        cout << "The Eight";
        break;
    case Seven:
        cout << "The Seven";
        break;
    case Six:
        cout << "The Six";
        break;
    case Five:
        cout << "The Five";
        break;
    case Four:
        cout << "The Four";
        break;
    case Three:
        cout << "The Three";
        break;
    case Two:
        cout << "The Two";
        break;
    }

    cout << " of ";

    switch (card.suit) {
    case Spades: {
        cout << "Spades" << endl;
        break;
    }
    case Hearts:
        cout << "Hearts" << endl;
        break;
    case Clubs:
        cout << "Clubs" << endl;
        break;
    case Diamonds:
        cout << "Diamonds" << endl;
        break;
    }
}

Card HighCard(Card card1, Card card2) {
    int value1 = card1.rank + card1.suit;
    int value2 = card2.rank + card2.suit;

    if (value1 > value2) {
        return card1;
    }
    else {
        return card2;
    }
}

int main()
{
	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	Card b;
	b.rank = Ace;
	b.suit = Spades;

	Card c;
	c.rank = Ace;
	c.suit = Diamonds;

	Card d;
	d.rank = Three;
	d.suit = Diamonds;

    PrintCard(c);
    PrintCard(HighCard(a,b));
	return 0;
}
